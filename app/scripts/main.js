var field1 = document.getElementById('field1');
var field2 = document.getElementById('field2');
var field3 = document.getElementById('field3');
var field4 = document.getElementById('field4');
var field5 = document.getElementById('field5');
var field6 = document.getElementById('field6');
var btnShuffle = $('#btnShuffle');
var numbers = [];

$('.circle').hide();

btnShuffle.click(function(e){
  e.preventDefault();

  var values = getInputsValue();
  console.log(values);

  if (checkIsNumber(values)) {
    if (checkIsEmpty(values)) {
      if (checkScope(values)) {
        if (redundant(values)) {
          var choose = chooseNumber();
          console.log(choose)
          var hits= checkHits(values, choose);

          $('.circle').show();
          var allNumber = allNumbers(choose);

          console.log(allNumber);

          var getData = getChartData(allNumber);
          chartJs(getData[0], getData[1]);

        }
        else {
          alert('Nie powtarzaj numerów');
        }
    }
      else {
          alert('Można wprowadzić numery od 1 do 49');
        }
    }
    else {
      alert('Nie zostawiaj pustych pól');
    }
  }
  else {
    alert('Musisz wprowadzić numer');
  }
});

function sortNumber(a,b) {
  return a - b;
}
function getChartData(allNumber) {
  var getX = [], getY = [], prev;

  allNumber.sort(sortNumber);
  
  console.log('sort '+allNumber);
  for (var i = 0; i < allNumber.length; i++) {
    if (allNumber[i] !== prev) {
      getX.push(allNumber[i]);
      getY.push(1);
    } else {
      getY[getY.length - 1]++;
    }
    prev = allNumber[i];
  }

  return [getX, getY]
}

function chartJs(dataXaxis, dataYaxis){
  Highcharts.chart('container', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Statystyka wylosowanych numerów'
    },
    xAxis: {
      categories: dataXaxis
    },
    yAxis: {
      title: {
        text: 'Ilość wylosowanego numeru'
      }
    },
    plotOptions: {
      line: {
        dataLabels: {
          enabled: true
        },
        enableMouseTracking: false
      }
    },
    series: [{
      name: 'Dany numer',
      data: dataYaxis
    }]
  });

}


function getInputsValue(){
  return [field1.value, field2.value, field3.value, field4.value, field5.value, field6.value];
}

function checkIsNumber(values){
  for(var i=0; i < values.length; i++){
    if (isNaN(values[i])){
      return false;
    }
  }
  return true;
}

function checkIsEmpty(values){
  for(var i=0; i < values.length; i++){
    if (values[i] == ""){
      return false;
    }
  }
  return true;
}

function checkScope(values){
  for(var i=0; i < values.length; i++){
    if(!(values[i] >= 1 && values[i]<=49)){
      return false;
    }
  }
  return true;
}

function redundant(values) {
  for (var i = 0; i < values.length; i++) {
    for (var k = 0; k < values.length; k++) {
      if (i != k && values[i] == values[k]) {
        return false;
      }
    }
  }
  return true;
}

function checkHits(values, drawnNumbers) {
  var hits = [];
  for (var i = 0; i < values.length; i++) {
    for (var j = 0; j < drawnNUmbers.length; k++) {
      if(values[i] == values[j]){
        hits.push(values[i]);
        }
      }
    }
  }

// wylosowane numery
function chooseNumber() {
  var points = [];
  for (var i = 0; i < 49; i++) {
    points[i] = i + 1;
  }
  var drawnNumbers = [];
  for (var y = 0; y < 6; y++) {
    var number = points[Math.floor(Math.random() * points.length)];
    points.splice(points.indexOf(number), 1);
    drawnNumbers[y] = number;

  }

  console.log(drawnNumbers);
  $('.circles:nth-child(1)').text(drawnNumbers[0]);
  $('.circles:nth-child(2)').text(drawnNumbers[1]);
  $('.circles:nth-child(3)').text(drawnNumbers[2]);
  $('.circles:nth-child(4)').text(drawnNumbers[3]);
  $('.circles:nth-child(5)').text(drawnNumbers[4]);
  $('.circles:nth-child(6)').text(drawnNumbers[5]);

  return drawnNumbers;
}

function checkHits(values, choose) {
  var hits = [];

  for (var i = 0; i < values.length; i++) {
    for (var j = 0; j < choose.length; j++) {
      if (values[i] == choose[j]) {
        hits.push(values[i]);

      }
    }
  }

  var result;
  switch(hits.length) {
    case 1:
      result = "Jedno trafienie!"
      break;
    case 2:
      result = "Dwa trafienia!"
      break;
    case 3:
      result = "Trzy trafienia!"
      break;
    case 4:
      result = "Cztery trafienia!"
      break;
    case 5:
      result = "Pięć trafień!"
      break;
    case 6:
      result = "Sześć trafień!"
      break;
    default:
      result = "Zero trafień :( "
  }
  $('.hit_box h1').text(result);

  //$('.hit_box ol').append('<li>' + result + '</li>');

  return hits;
}

function allNumbers(choose) {
  numbers = numbers.concat(choose);
  return numbers;
}


